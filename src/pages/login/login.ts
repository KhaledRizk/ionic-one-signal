import { Component } from '@angular/core';
import {IonicPage, MenuController, NavController, NavParams, ToastController} from 'ionic-angular';
import { AngularFireAuth } from 'angularfire2/auth';
import {User} from "../../models/user";
import {AdminPage} from "../admin/admin";
import {UserPage} from "../user/user";
import {RegisterPage} from "../register/register";
import {UserProvider} from "../../providers/user/user";


@IonicPage()
@Component({
  selector: 'page-login',
  templateUrl: 'login.html',
})
export class LoginPage {

  user = {} as User;

  constructor(private userProvider: UserProvider, private menu: MenuController, private toastController: ToastController, private fireAuth: AngularFireAuth, public navCtrl: NavController, public navParams: NavParams) {
  }

  ionViewDidLoad() {
    console.log('ionViewDidLoad LoginPage');
  }

  async login(user: User){

    try {
      const info = await this.fireAuth.auth.signInWithEmailAndPassword(user.email, user.password);
      console.log(info);
      if (info.user.emailVerified) {
        let res = user.email.split("@");
        if(res[1] == "ibtkarqa.com")
          await this.navCtrl.setRoot(AdminPage);
        else
          await this.navCtrl.setRoot(UserPage);
      }
      else{
        let toast
        this.toastController.create({
          message: "Email is not verified",
          duration: 3000,
          position: 'bottom'
        }).present();
      }
    }
    catch(e){
      let toast
      console.log(e);
      if(e.message.includes(": "))
        toast = this.toastController.create({
          message: e.message.split(": ")[1],
          duration: 3000,
          position: 'bottom'
        });
      else
        toast = this.toastController.create({
          message: e.message,
          duration: 3000,
          position: 'bottom',
        });
      toast.present();
    }
  }

  register(){
    this.navCtrl.push(RegisterPage);
  }

  ionViewDidEnter() {
    this.menu.enable(false);
  }

  ionViewWillLeave() {
    this.menu.enable(true);
  }


}
