import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams } from 'ionic-angular';
import {QuotationFormPage} from "../quotation-form/quotation-form";
import { AngularFirestore, AngularFirestoreCollection, AngularFirestoreDocument } from '@angular/fire/firestore';
import { AngularFireAuth } from 'angularfire2/auth';


/**
 * Generated class for the UserPage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */

@IonicPage()
@Component({
  selector: 'page-user',
  templateUrl: 'user.html',
})
export class UserPage {

  private quotationsCollection: AngularFirestoreCollection<any>;
  quotations = [];
  email = null;

  constructor(private fireAuth: AngularFireAuth, private afs: AngularFirestore, public navCtrl: NavController, public navParams: NavParams) {
    this.fireAuth.auth.onAuthStateChanged((user) =>{
      this.email = user.email;
    });
    this.quotationsCollection = afs.collection<any>('quotations');
    this.quotationsCollection.get().subscribe((querySnapshot) => {
      querySnapshot.forEach((doc) => {
        let t = doc.data();
        if (t.email == this.email) {
          t.key = doc.id;
          this.quotations.push(t);
        }
      });
    })
  }

  ionViewDidLoad() {
    console.log('ionViewDidLoad UserPage');
  }

  goToQuotation(){
    this.navCtrl.push(QuotationFormPage);
  }

}
