import {Component, ViewChild} from '@angular/core';
import {IonicPage, NavController, NavParams, ToastController} from 'ionic-angular';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import {QuotationProvider} from "../../providers/quotation/quotation";
import {UserPage} from "../user/user";
import { AngularFireAuth } from 'angularfire2/auth';


@IonicPage()
@Component({
  selector: 'page-quotation-form',
  templateUrl: 'quotation-form.html',
})
export class QuotationFormPage {

  quotation = {
    category: String,
    name: String,
    desc: String,
    type: String,
    email: ""
  }

  @ViewChild('signupSlider') signupSlider: any;

  slideOneForm: FormGroup;
  slideTwoForm: FormGroup;
  submitAttempt: boolean = false;

  constructor(private fireAuth: AngularFireAuth, private toastController: ToastController, private quotationProvider: QuotationProvider, public navCtrl: NavController, public navParams: NavParams, private formBuilder: FormBuilder) {
    this.fireAuth.auth.onAuthStateChanged((user) =>{
      this.quotation.email = user.email;
    });

    this.slideOneForm = formBuilder.group({
      categ: ['', Validators.required],
    });

    this.slideTwoForm = formBuilder.group({
      name: ['', Validators.compose([Validators.required, Validators.pattern('[a-zA-Z]*')])],
      desc: ['', Validators.required],
      subtype: ['', Validators.required]
    });
  }

  ionViewDidLoad() {
    console.log('ionViewDidLoad QuotationFormPage');
  }

  next(){
    this.signupSlider.slideNext();
  }

  prev(){
    this.signupSlider.slidePrev();
  }

  save(){
    this.submitAttempt = true;

    if(!this.slideOneForm.valid){
      this.signupSlider.slideTo(0);
    }
    else if(!this.slideTwoForm.valid){
      this.signupSlider.slideTo(1);
    }
    else {
      this.quotation.category = this.slideOneForm.value.categ;
      this.quotation.name = this.slideTwoForm.value.name;
      this.quotation.desc = this.slideTwoForm.value.desc;
      this.quotation.type = this.slideTwoForm.value.subtype;
      this.quotationProvider.addquotation(this.quotation);
      this.navCtrl.setRoot(UserPage);
    }

  }

}
