import { Component } from '@angular/core';
import {IonicPage, NavController, NavParams, ToastController} from 'ionic-angular';
import {User} from "../../models/user";
import { AngularFireAuth } from 'angularfire2/auth';
import {LoginPage} from "../login/login";
import {UserProvider} from "../../providers/user/user";


/**
 * Generated class for the RegisterPage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */

@IonicPage()
@Component({
  selector: 'page-register',
  templateUrl: 'register.html',
})
export class RegisterPage {

  user = {} as User;

  constructor(private userProvider: UserProvider, private toastController: ToastController, private fireAuth: AngularFireAuth, public navCtrl: NavController, public navParams: NavParams) {
  }

  ionViewDidLoad() {
    console.log('ionViewDidLoad RegisterPage');
  }

  async register(user: User){
    try {
      const info = await this.fireAuth.auth.createUserWithEmailAndPassword(user.email, user.password);

      if (info) {
        this.userProvider.addUser(this.user);
        this.fireAuth.auth.currentUser.sendEmailVerification().then(function() {
          // Email sent.
        }).catch(function(error) {
          // An error happened.
        });

        this.navCtrl.push(LoginPage);
      }
    }
    catch(e){
      console.log(e);
      let toast;
      if(e.message.includes(": "))
        toast = this.toastController.create({
          message: e.message.split(": ")[1],
          duration: 3000,
          position: 'bottom'
        });
      else
        toast = this.toastController.create({
          message: e.message,
          duration: 3000,
          position: 'bottom'
        });
      toast.present();
    }

  }

}
