import { Component } from '@angular/core';
import {NavController, NavParams} from 'ionic-angular';
import {LoginPage} from "../login/login";
import { AngularFireAuth } from 'angularfire2/auth';
import {UserPage} from "../user/user";
import {AdminPage} from "../admin/admin";


@Component({
  selector: 'page-home',
  templateUrl: 'home.html'
})
export class HomePage {

  constructor(private fireAuth: AngularFireAuth, public navCtrl: NavController, public navParams: NavParams) {
  }

  splash = true;

  ionViewDidLoad() {
    setTimeout(() => this.splash = false, 4000);
  }

  ionViewDidEnter() {
    this.fireAuth.auth.onAuthStateChanged((user) => {
      if (user) {
        console.log("this"+user.toString)
        if(user.emailVerified){
          let res = user.email.split("@");
          if(res[1] == "ibtkarqa.com")
            this.navCtrl.setRoot(AdminPage);
        else
            this.navCtrl.setRoot(UserPage);
        }
      } else {
        this.navCtrl.setRoot(LoginPage)
      }
    });
  }

}
