import { Component } from '@angular/core';
import {IonicPage, NavController, NavParams, ToastController} from 'ionic-angular';
import {Observable} from "rxjs";
import { AngularFirestore, AngularFirestoreCollection, AngularFirestoreDocument } from '@angular/fire/firestore';
import {LocalNotifications} from "@ionic-native/local-notifications";
import {AlertController, Platform} from "ionic-angular";

/**
 * Generated class for the AdminPage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */

@IonicPage()
@Component({
  selector: 'page-admin',
  templateUrl: 'admin.html',
})
export class AdminPage {

  private quotationsCollection: AngularFirestoreCollection<any>;
  quotations = [];
  price = null;

  constructor(private toastController: ToastController, private afs: AngularFirestore, public navCtrl: NavController, public navParams: NavParams) {
    this.quotationsCollection = afs.collection<any>('quotations');
    this.quotationsCollection.get().subscribe((querySnapshot) => {
      querySnapshot.forEach((doc) => {
        let t = doc.data();
        if(t.price == undefined) {
          t.key = doc.id;
          this.quotations.push(t);
        }
      });
    });
  }

  ionViewDidLoad() {
    console.log('ionViewDidLoad AdminPage');
  }

  setPrice(quotation){
    if(!(this.price == "") && this.price != null) {
      quotation.price = this.price;
      this.quotationsCollection.doc(quotation.key).update(quotation);
      for(let i = 0; i < this.quotations.length; i++)
        if(this.quotations[i].key == quotation.key)
          this.quotations.splice(i, 1);
      this.toastController.create({
        message: "Price Updated Correctly.",
        duration: 3000,
        position: 'bottom'
      }).present();
    }
    else{
      this.toastController.create({
        message: "Please Enter A Valid Value.",
        duration: 3000,
        position: 'bottom'
      }).present();
    }
  }

}
