import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import {User} from "../../models/user";
import {Observable} from "rxjs";
import { AngularFirestore, AngularFirestoreCollection, AngularFirestoreDocument } from '@angular/fire/firestore';
import {LocalNotifications} from "@ionic-native/local-notifications";
import {AlertController, Platform} from "ionic-angular";


/*
  Generated class for the QuotationProvider provider.

  See https://angular.io/guide/dependency-injection for more info on providers
  and Angular DI.
*/
@Injectable()
export class QuotationProvider {

  private quotationsCollection: AngularFirestoreCollection<any>;
  quotations: Observable<any>;

  constructor( private afs: AngularFirestore,  public localNotifications: LocalNotifications,
    public platform: Platform,
    public alertCtrl: AlertController) {
    this.quotationsCollection = afs.collection<any>('quotations');
    this.quotations = this.quotationsCollection.valueChanges();
  }
  addquotation(quotation) {
    this.quotationsCollection.add(quotation);
    this.localNotifications.schedule({
      text: 'Quotation Submitted.',
      led: 'FF0000',
      sound: this.setSound(),
      foreground: true
    });

  }


  private setSound() {
    if (this.platform.is('android')) {
      return 'file://assets/sounds/Rooster.mp3'
    } else {
      return 'file://assets/sounds/Rooster.caf'
    }
  }
}
