import { Injectable } from '@angular/core';
import { AngularFirestore, AngularFirestoreCollection, AngularFirestoreDocument } from '@angular/fire/firestore';
import { Observable } from 'rxjs';
import {User} from "../../models/user";


/*
  Generated class for the UserProvider provider.

  See https://angular.io/guide/dependency-injection for more info on providers
  and Angular DI.
*/
@Injectable()
export class UserProvider {

  private userCollection: AngularFirestoreCollection<User>;
  users: Observable<User[]>;

  constructor( private afs: AngularFirestore) {
    this.userCollection = afs.collection<User>('users');
    this.users = this.userCollection.valueChanges();
  }

  addUser(item: User) {
    this.userCollection.add(item);

  }

  getUser(email){
    this.userCollection.get().subscribe((querySnapshot) => {
      querySnapshot.forEach((doc) => {
        console.log(`${doc.id} => ${doc.data()}`);
      });
    });
  }


}
