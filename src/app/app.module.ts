import { BrowserModule } from '@angular/platform-browser';
import { ErrorHandler, NgModule } from '@angular/core';
import { IonicApp, IonicErrorHandler, IonicModule } from 'ionic-angular';

import { MyApp } from './app.component';
import { HomePage } from '../pages/home/home';
import { ListPage } from '../pages/list/list';

import { StatusBar } from '@ionic-native/status-bar';
import { SplashScreen } from '@ionic-native/splash-screen';
import { AngularFireModule } from 'angularfire2';
import { AngularFireAuthModule } from 'angularfire2/auth';
import { AngularFirestoreModule } from '@angular/fire/firestore';
import { AngularFireStorageModule } from '@angular/fire/storage';
import {FIREBASE_INFO} from "./firebase.info";
import {LoginPage} from "../pages/login/login";
import {UserPage} from "../pages/user/user";
import {AdminPage} from "../pages/admin/admin";
import {RegisterPage} from "../pages/register/register";
import { UserProvider } from '../providers/user/user';
import {QuotationFormPage} from '../pages/quotation-form/quotation-form';
import { QuotationProvider } from '../providers/quotation/quotation';
import { LocalNotifications } from '@ionic-native/local-notifications';
import {OneSignal} from "@ionic-native/onesignal";



@NgModule({
  declarations: [
    MyApp,
    HomePage,
    ListPage,
    LoginPage,
    UserPage,
    AdminPage,
    RegisterPage,
    QuotationFormPage
  ],
  imports: [
    BrowserModule,
    IonicModule.forRoot(MyApp),
    AngularFireAuthModule,
    AngularFireModule.initializeApp(FIREBASE_INFO),
    AngularFirestoreModule,
    AngularFireStorageModule
  ],
  bootstrap: [IonicApp],
  entryComponents: [
    MyApp,
    HomePage,
    ListPage,
    LoginPage,
    UserPage,
    AdminPage,
    RegisterPage,
    QuotationFormPage
  ],
  providers: [
    StatusBar,
    SplashScreen,
    {provide: ErrorHandler, useClass: IonicErrorHandler},
    UserProvider,
    QuotationProvider,
    LocalNotifications,
    OneSignal
  ]
})
export class AppModule {}
