import { Component, ViewChild } from '@angular/core';
import { Nav, Platform } from 'ionic-angular';
import { StatusBar } from '@ionic-native/status-bar';
import { SplashScreen } from '@ionic-native/splash-screen';

import { HomePage } from '../pages/home/home';
import { ListPage } from '../pages/list/list';
import { AngularFireAuth } from 'angularfire2/auth';
import {User} from "../models/user";
import {UserProvider} from "../providers/user/user";
import {Observable} from "rxjs";
import { OneSignal } from '@ionic-native/onesignal';


@Component({
  templateUrl: 'app.html'
})
export class MyApp {
  @ViewChild(Nav) nav: Nav;

  rootPage: any = HomePage;

  pages: Array<{title: string, component: any}>;

  user = {email: "placeholder",
          password: "placeholder",
          userName: "placeholder"} as User;

  constructor(private userProvider: UserProvider, private fireAuth: AngularFireAuth, public platform: Platform, public statusBar: StatusBar, public splashScreen: SplashScreen) {
    this.initializeApp();

    // used for an example of ngFor and navigation
    this.pages = [
      { title: 'Home', component: HomePage },
      { title: 'List', component: ListPage }
    ];

  }

  initializeApp() {
    this.platform.ready().then(() => {
      // Okay, so the platform is ready and our plugins are available.
      // Here you can do any higher level native things you might need.
      this.statusBar.styleDefault();
      this.splashScreen.hide();
      this.fireAuth.auth.onAuthStateChanged((user) =>{
        this.user.email = user.email;
      });
      /*
var notificationOpenedCallback = function(jsonData) {
  console.log('notificationOpenedCallback: ' + JSON.stringify(jsonData));
};

window["plugins"].OneSignal
  .startInit("ionictest-ddb50", "AIzaSyB_2zGLi-FuAA0Uv6Ro9Xu-ewq5oK8myME")
  .handleNotificationOpened(notificationOpenedCallback)
  .endInit();
  */
    });
  }

  openPage(page) {
    // Reset the content nav to have just this page
    // we wouldn't want the back button to show in this scenario
    this.nav.setRoot(page.component);
  }

  signOut(){
    this.fireAuth.auth.signOut();
  }
}
